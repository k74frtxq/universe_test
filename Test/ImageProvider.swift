//
//  ImageProvider.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import Foundation
import RxSwift

struct ImageProviderContext {
  
  public let bitmap: CGRect
  
  public init(origin: CGPoint, size: CGSize) {
    bitmap = CGRect(origin: origin, size: size)
  }
  
  static let shared = ImageProviderContext(
    origin: .zero,
    size: CGSize(width: 2000, height: 2000)
  )
}

class ImageProvider: NSObject {
  
  let renderer: UIGraphicsImageRenderer
  let context: ImageProviderContext
  
  let relay: PublishSubject<UIImage>
  let queue: OperationQueue
  
  var token: NSKeyValueObservation?
  let lock = NSRecursiveLock()
  
  init(context: ImageProviderContext = .shared) {
    self.context = context
    
    let format = UIGraphicsImageRendererFormat()
    format.opaque = true
    renderer = UIGraphicsImageRenderer(
      size: context.bitmap.size,
      format: format
    )
    
    relay = PublishSubject<UIImage>()
    queue = OperationQueue()
    queue.qualityOfService = .userInteractive
    queue.maxConcurrentOperationCount = 12
    
    token = queue.observe(\.maxConcurrentOperationCount, options: [.initial, .old, .new], changeHandler: { (_queue, change) in
      let concurrentOperationCount = change.newValue ?? 0
      print("Rendering \(concurrentOperationCount) images concurrently")
    })
  }
  
  func imageStream() -> Observable<UIImage> {
    relay.asObserver()
  }
  
  func requestImage(index: Int) {
    queue.addOperation {
      let image = self.renderer.image { (rendererContext) in
        UIColor.white.setFill()
        rendererContext.cgContext.fill(self.context.bitmap)
        let circlePath = UIBezierPath(ovalIn: self.context.bitmap)
        rendererContext.cgContext.setLineWidth(10)
        rendererContext.cgContext.addPath(circlePath.cgPath)
        UIColor.red.setStroke()
        circlePath.stroke()
        
        let string = NSString(string: "\(index)")
        string.draw(at: .init(x: self.context.bitmap.midX, y: self.context.bitmap.midY),
                    withAttributes: [.foregroundColor: UIColor.black,
                                     .font: UIFont.systemFont(ofSize: 30, weight: .bold)])
      }
      self.lock.lock()
      self.relay.on(.next(image))
      self.lock.unlock()
    }
  }
}
