//
//  ViewController.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
  
  let provider = ImageProvider()
  let saver = ImageSaver()
  
  let disposeBag = DisposeBag()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    provider.imageStream().debug().flatMap {
      self.saver.saveImage($0)
    }
    .buffer(timeSpan: DispatchTimeInterval.seconds(1), count: 10, scheduler: MainScheduler.asyncInstance)
    .subscribe(onNext: { results in
      let saveResult = results.reduce(into: (0, 0)) { (counter, result) in
        switch result {
        case .failure:
          counter.0 += 1
        case .success:
          counter.1 += 1
        }
      }
      print("Operation complete. Failed - \(saveResult.0); Saved - \(saveResult.1)")
    }).disposed(by: disposeBag)
    
    (0..<1000).forEach { index in self.provider.requestImage(index: index) }
  }
}
